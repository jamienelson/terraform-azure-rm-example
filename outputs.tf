output "build_key" {
	value = "${var.build_key}"
}
output "environment" {
 value = "${var.environment_label}"
}
output "region" {
	value = "${azurerm_resource_group.production.location}"
}
output "resourcegroupname" {
	value = "${azurerm_resource_group.production.name}"
}
output "storageaccountname" {
	value = "${azurerm_storage_account.production.name}"
}
#No Image_URI needed, as it comes from packer

output "server_ips" {
	value = "${join(",",azurerm_public_ip.ips.*.ip_address)}"
}

output "instance_names" {
	value = "${join(",",azurerm_virtual_machine.win.*.name)}"
}

output "admin_user" {
	value = "${var.admin_user}"
}
output "admin_pass" {
	value = "${var.admin_pass}"
}

output "num_servers" {
	value = "${var.num_servers}"
}
## Provider Details
