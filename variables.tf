#TF_VAR_<variable names can be read in


variable "build_key" {
  default = "client01"
}
variable "provider_key" {
  description = ""
  default = "subscription01"
}

variable "vm_winrm_port" {
  description = "WinRM Public Port"
  default = "5986"
}
variable "admin_user" {
	default = "superadmin"
}
variable "admin_pass" {
	default = "SomePassword4"
}

variable "environment_label" {
	default	= "staging"
}

variable "subscription_id" {

}
variable "client_id" {

}
variable "client_secret" {

}
variable "tenant_id" {

}

##environment details
variable "num_servers" {
  default = "1"
}
variable "instance_names" {
  default = "windows01"
}

variable "region" {
  default = "australiasoutheast"
}

variable "vm_size" {
  default = "Standard_DS2"
}

variable "disk_size_gb" {
  default = "120"
}

variable "storage_account_type" {
  default = "Standard_GRS"
}

variable "imageoffer" {
  default = "WindowsServer"
}

variable "imagepublisher" {
  default = "MicrosoftWindowsServer"
}

variable "imagesku" {
  default = "2012-R2-Datacenter"
}
