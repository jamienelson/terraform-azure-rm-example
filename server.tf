resource "azurerm_virtual_machine" "win" {
	count 		            = "${var.num_servers}"
	name                  = "${lower(format("%.12s", element(split("\n",var.instance_names), count.index)))}"
	location              = "${var.region}"
	resource_group_name   = "${azurerm_resource_group.production.name}"
	network_interface_ids = ["${element(azurerm_network_interface.win_net.*.id,count.index)}"]
	vm_size               = "${var.vm_size}"
	delete_os_disk_on_termination = "true"


  storage_image_reference {
    publisher = "${var.imagepublisher}"
    offer     = "${var.imageoffer}"
    sku       = "${var.imagesku}"
    version   = "latest"
  }

  storage_os_disk {
    name            = "${lower(format("osdisk-%.15s", element(split("\n",var.instance_names), count.index)))}"
    vhd_uri 		    = "${azurerm_storage_account.production.primary_blob_endpoint}${azurerm_storage_container.production.name}/${element(split("\n",var.instance_names), count.index)}.vhd"
    caching       	= "ReadWrite"
    os_type 		    = "windows"
    create_option 	= "FromImage"
    disk_size_gb	  = "${var.disk_size_gb}"
  }
  boot_diagnostics {
    enabled = true
    storage_uri = "${azurerm_storage_account.production.primary_blob_endpoint}"
  }

  os_profile {
    computer_name  	= "${lower(format("%.14s", element(split("\n",var.instance_names), count.index)))}"
    admin_username 	= "${var.admin_user}"
    admin_password	= "${var.admin_pass}"
        #Include Deploy.PS1 with variables injected as custom_data
        custom_data = "${base64encode("Param($RemoteHostName = \"${element(azurerm_public_ip.ips.*.ip_address, count.index)}\", $ComputerName = \"${lower(format("%.14s", element(split("\n",data.consul_keys.instance_names.var.instance_names), count.index)))}\", $WinRmPort = ${var.vm_winrm_port}) ${file("Deploy.PS1")}")}"
      }

      os_profile_windows_config {
        provision_vm_agent        = true
        enable_automatic_upgrades = true

        additional_unattend_config {
          pass = "oobeSystem"
          component = "Microsoft-Windows-Shell-Setup"
          setting_name = "AutoLogon"
          content = "<AutoLogon><Password><Value>${var.admin_pass}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_user}</Username></AutoLogon>"
        }
        #Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
        additional_unattend_config {
          pass          = "oobeSystem"
          component     = "Microsoft-Windows-Shell-Setup"
          setting_name  = "FirstLogonCommands"
          content       = "${file("FirstLogonCommands.xml")}"
        }
      } 

      provisioner "remote-exec" {
        inline = [
        "powershell.exe -sta -ExecutionPolicy Unrestricted echo 'i can run tasks here, such as bootstrapping management tools'"
        ]
        connection {
         type     = "winrm"
         https    = true
         insecure = true
         user     = "${var.admin_user}"
         password = "${var.admin_pass}"
         host     = "${element(azurerm_public_ip.ips.*.ip_address, count.index)}"
         port     = "${var.vm_winrm_port}"
       }
     }

     tags {
      build_key 		= "${var.build_key}"
      environment 	= "${var.environment_label}"
    }
  }

  resource "azurerm_public_ip" "ips" {
   count 		           = "${var.num_servers}"
   name 		           = "${format("ip-%.12s%.10s",lower(var.build_key),lower(element(split("\n",var.instance_names), count.index)))}"
   location 	         = "${var.region}"
   resource_group_name = "${azurerm_resource_group.production.name}"
   public_ip_address_allocation = "static"

   tags {
    environment = "${var.environment_label}"
    build_key   = "${var.build_key}"
  }
}

resource "azurerm_network_interface" "win_net" {
	count 		          = "${var.num_servers}"
	name 			          = "${format("inet-%.12s%.10s",lower(var.build_key),lower(element(split("\n",var.instance_names), count.index)))}"
	location            = "${var.region}"
	resource_group_name = "${azurerm_resource_group.production.name}"
	network_security_group_id = "${azurerm_network_security_group.vm_security_group.id}"

	ip_configuration {
		name                          = "subnet_config"
		subnet_id                     = "${azurerm_subnet.subnet1.id}"
		private_ip_address_allocation = "dynamic"
		public_ip_address_id          = "${element(azurerm_public_ip.ips.*.id,count.index)}"
	}
}

resource "azurerm_network_security_group" "vm_security_group" {
  name                = "${var.build_key}-sg"
  location            = "${var.region}"
  resource_group_name = "${azurerm_resource_group.production.name}"

  tags {
    environment = "${var.environment_label}"
    build_key   = "${var.build_key}"
  }
}

resource "azurerm_network_security_rule" "rdpRule" {
  name                        = "rdpRule"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "3389"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.production.name}"
  network_security_group_name = "${azurerm_network_security_group.vm_security_group.name}"
}

resource "azurerm_network_security_rule" "winrmRule" {
  name                        = "winrmRule"
  priority                    = 110
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "${var.vm_winrm_port}"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.production.name}"
  network_security_group_name = "${azurerm_network_security_group.vm_security_group.name}"
}