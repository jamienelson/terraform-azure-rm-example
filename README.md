# Terraform example of creating a Windows Server using AzureRM

This repo creates a:
1. resource group
2. a storage account
3. a network
4. a public IP
5. a Windows server, and runs a remote bootstrap command on it

It also tags resources and names them based on details from the variables file.

To use this you need at a minimum the 
1.  subscription_id 
2.  client_id       
3.  client_secret   
4.  tenant_id  

For instructions on how to get these details if you don't have them a walkthrough is here:
https://www.terraform.io/docs/providers/azurerm/#creating-credentials     
