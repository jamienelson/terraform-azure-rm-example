# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}


# Create a resource group
resource "azurerm_resource_group" "production" {
  name     = "${format("%.30s%.30s",lower(var.build_key),lower(var.environment_label))}"
  location = "${var.region}"
}

# Create a virtual network in the web_servers resource group
resource "azurerm_virtual_network" "network" {
  name                = "${format("%.12s%.10s",lower(var.build_key),lower(var.environment_label))}"
  address_space       = ["10.0.0.0/16"]
  location            = "${var.region}"
  resource_group_name = "${azurerm_resource_group.production.name}"

}
# create subnet
resource "azurerm_subnet" "subnet1" {
  name = "subnet1"
  resource_group_name = "${azurerm_resource_group.production.name}"
  virtual_network_name = "${azurerm_virtual_network.network.name}"
  address_prefix = "10.0.2.0/24"
}

#Storage account must be unique, so this could error if it isn't
resource "azurerm_storage_account" "production" {
  name = "${format("%.12s%.10s",lower(var.build_key),lower(var.environment_label))}"
  resource_group_name = "${azurerm_resource_group.production.name}"
  location = "${azurerm_resource_group.production.location}"
  account_type = "${var.storage_account_type}"
  tags {
    environment = "${var.environment_label}"
    build_key   = "${var.build_key}"
  }
}
resource "azurerm_storage_container" "production" {
  name                  = "vhds"
  resource_group_name   = "${azurerm_resource_group.production.name}"
  storage_account_name  = "${azurerm_storage_account.production.name}"
  container_access_type = "private"
}